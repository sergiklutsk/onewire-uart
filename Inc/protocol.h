/*
 * protocol.h
 *
 *  Created on: 21 ���. 2021 �.
 *      Author: Sergiklutsk
 *
 * Json types example:
 * {"type":"set_profile","profile":1,"min_temp":24,"max_temp":30,"power":2}
 * {"type":"set_profile","profile":1,"min_temp":24}
 *
 * {"type":"get_cfg"}
 * {"type":"get_sensor"}
 * {"type":"change_profile","profile":2}
 */

#ifndef PROTOCOL_H_
#define PROTOCOL_H_

#define TYPE__JSON_OBJECT_NAME			"type"
#define SET_HYST__JSON_TYPE_NAME		"set_profile"
#define GET_CFG__JSON_TYPE_NAME			"get_cfg"
#define TEMP_SENSOR__JSON_TYPE_NAME		"get_sensor"
#define CHANGE_PROFILE__JSON_TYPE_NAME	"change_profile"

#define ID__JSON_OBJECT_NAME		"id"
#define PROFILE__JSON_OBJECT_NAME	"profile"
#define MIN_TEMP__JSON_OBJECT_NAME	"min_temp"
#define MAX_TEMP__JSON_OBJECT_NAME	"max_temp"
#define POWER__JSON_OBJECT_NAME		"power"
#define PROFILES__JSON_OBJECT_NAME	"profiles"
#define TEMP__JSON_OBJECT_NAME		"temp"

static const char* JSON_ERRORS_STR[] = {
	"success",
	"fail",
	"tokens_not_found",
	"json_param_fail",
	"json_type_error",
	"json_profile_error",
	"json_mintemp_error",
	"json_maxtemp_error",
	"json_power_error"
};
extern enum jsonError {
	JSON_OK,
	JSON_FAIL,
	JSON_TOKENS_NOT_FOUND,
	JSON_PARAMS_FAIL,
	JSON_TYPE_ERROR,
	JSON_PROFILE_ERROR,
	JSON_MINTEMP_FAIL,
	JSON_MAXTEMP_FAIL,
	JSON_POWER_FAIL
} jsonError;

#include "stdio.h"

uint8_t jsonCommandsParser(char* jsonString);
void setHystProfile_Json(char *jsonString);
void sendDefaultResponse_Json(const char* messType, uint8_t err);
void makeConfig_Json();
void getTemp_Json();
void sendMessage(char * message);

#endif /* PROTOCOL_H_ */
