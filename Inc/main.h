/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "OneWire.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
Temperature readTemp(uint8_t id);

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_Pin GPIO_PIN_13
#define LED_GPIO_Port GPIOC
#define BUTTON_UP_Pin GPIO_PIN_10
#define BUTTON_UP_GPIO_Port GPIOB
#define BUTTON_ONOFF_Pin GPIO_PIN_11
#define BUTTON_ONOFF_GPIO_Port GPIOB
#define BUTTON_DOWN_Pin GPIO_PIN_14
#define BUTTON_DOWN_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
void MX_USART2_UART_Init(uint32_t baud);
void get_ROMid (void);
void button_press(uint8_t button);
void SetPower(uint8_t power);
void CheckAndSetPower(float celsium);
void ReadConstantsFromEEP(void);
void WriteDefaultsConstantToEEP(void);
void WriteValuesToEEprom(void);
uint8_t USB_ReceiveData_Callback (uint8_t* Buf, uint32_t *Len);

#define CHECK_DATA	887
#define EEP_HYST_MIN	1
#define EEP_HYST_MAX	2
#define EEP_HYST_POWER	3
#define EEP_PROFILE		4

#define EEP_CHECK_ADDR	10

#define MAX_PROFILES 4

#define OW_USART USART2
#define MAXDEVICES_ON_THE_BUS 6
#define LED_ONOFF();  GPIOC->ODR ^= LED_Pin;
#define ONOFF_BUTTON 1
#define UP_BUTTON 2
#define DOWN_BUTTON 3
#define DELAY1_MS	60
#define MAX_UPDATE_CNT 1

#define  BUFFER_SIZE 2048

// USART definition
#define MAXSTRING 20
#define UART_PACKET_OK 0
#define UART_PACKET_TOO_LONG 1
#define UART_PACKET_CORRUPT 2
#define UART_BUSY 1
#define UART_CPLT 0

typedef struct hysteresis
{
	float min_temp;
	float max_temp;
	uint8_t power;
	uint8_t changed;
} hysteresis;
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
