/*
 * protocol.c
 *
 *  Created on: 21 ���. 2021 �.
 *      Author: User
 */

#include "main.h"
#include "OneWire.h"
#include "string.h"
#include "stdlib.h"
#include "stdbool.h"
#include "protocol.h"
#include "jsmn.h"
#include "json-maker.h"

extern UART_HandleTypeDef huart1;
extern uint8_t profileIndex;

int r;
jsmn_parser p;
jsmntok_t tokens[128];
jsmntok_t *g = NULL;

static int jsoneq(const char *json, jsmntok_t *tok, const char *s);

uint8_t jsonCommandsParser(char* jsonString) {

	uint8_t err = JSON_PARAMS_FAIL;
	uint8_t i = 0;

	char strBuffer[100] = { 0 };

	jsmn_init(&p);
	r = jsmn_parse(&p, jsonString, strlen(jsonString), tokens, sizeof(tokens) / sizeof(tokens[0]));

	if (r < 0) {
		err = JSON_TOKENS_NOT_FOUND;
	}
	else {
		for (i = 0; i < r; i++) {
			if (jsoneq(jsonString, &tokens[i], TYPE__JSON_OBJECT_NAME) == 0) {
				sprintf(strBuffer, "%.*s", tokens[i + 1].end - tokens[i + 1].start, jsonString + tokens[i + 1].start);
				//HAL_UART_Transmit(&huart1,(uint8_t*)strBuffer,strlen(strBuffer), HAL_MAX_DELAY);//sendMessage(strBuffer);
				/*
				 * Parse message type'set_hyst'
				 **/
				if (!memcmp(strBuffer, SET_HYST__JSON_TYPE_NAME, strlen(strBuffer))) {
					setHystProfile_Json(jsonString);
					err = JSON_OK;
					break;
				}
				/*
				 * Parse message type'get_cfg'
				 **/
				else if (!memcmp(strBuffer, GET_CFG__JSON_TYPE_NAME, strlen(GET_CFG__JSON_TYPE_NAME))) {
						makeConfig_Json();
						err = JSON_OK;
						break;
				}
				/*
				 * Parse message type'temp_sensor'
				 **/
				else if (!memcmp(strBuffer, TEMP_SENSOR__JSON_TYPE_NAME, strlen(TEMP_SENSOR__JSON_TYPE_NAME))) {
						getTemp_Json();
						err = JSON_OK;
						break;
				}
				/*
				 * Parse message type'change_profile'
				 **/
				else if (!memcmp(strBuffer, CHANGE_PROFILE__JSON_TYPE_NAME, strlen(CHANGE_PROFILE__JSON_TYPE_NAME))) {
					for (uint8_t j = i + 1; j < r; j++) {
						if (jsoneq(jsonString, &tokens[j], PROFILE__JSON_OBJECT_NAME) == 0) {
							sprintf(strBuffer, "%.*s", tokens[j + 1].end - tokens[j + 1].start, jsonString + tokens[j + 1].start);
							if (strlen(strBuffer) > 0) {
								uint8_t value = atoi(strBuffer);
								if (value > 0 && value < MAX_PROFILES + 1) {
									profileIndex = value - 1;
									err = JSON_OK;
									sendDefaultResponse_Json(CHANGE_PROFILE__JSON_TYPE_NAME, err);
									WriteValuesToEEprom();
								}
							}
						}
					}
					break;
				} else err = JSON_TYPE_ERROR;
			}
		}
	}

	if (err != JSON_OK) {
		sendDefaultResponse_Json(TYPE__JSON_OBJECT_NAME, err);
	}

	return err;
}

void setHystProfile_Json(char *jsonString) {

	extern hysteresis hystProfiles[MAX_PROFILES];

	char strBuffer[BUFFER_SIZE] = { 0 };
	bool saveFlag = true;
	uint32_t minTemp = 0, maxTemp = 0;
	uint8_t power = 0, profile = 0;
	uint8_t err = JSON_OK;

	for (uint8_t i = 0; i < r; i++) {
		if (jsoneq(jsonString, &tokens[i], PROFILE__JSON_OBJECT_NAME) == 0) {
			sprintf(strBuffer, "%.*s", tokens[i + 1].end - tokens[i + 1].start, jsonString + tokens[i + 1].start);
			profile = atoi(strBuffer);
			if (profile < 1) {
				err = JSON_PROFILE_ERROR;
				break;
			}
			i++;
		}
		else if (jsoneq(jsonString, &tokens[i], MIN_TEMP__JSON_OBJECT_NAME) == 0) {
			sprintf(strBuffer, "%.*s", tokens[i + 1].end - tokens[i + 1].start, jsonString + tokens[i + 1].start);
			minTemp = atoi(strBuffer);
			if (minTemp < 1) {
				err = JSON_MINTEMP_FAIL;
				break;
			}
			i++;
		}
		else if (jsoneq(jsonString, &tokens[i], MAX_TEMP__JSON_OBJECT_NAME) == 0) {
			sprintf(strBuffer, "%.*s", tokens[i + 1].end - tokens[i + 1].start, jsonString + tokens[i + 1].start);
			maxTemp = atoi(strBuffer);
			if (maxTemp < 1) {
				err = JSON_MAXTEMP_FAIL;
				break;
			}
			i++;
		}
		else if (jsoneq(jsonString, &tokens[i], POWER__JSON_OBJECT_NAME) == 0) {
			sprintf(strBuffer, "%.*s", tokens[i + 1].end - tokens[i + 1].start, jsonString + tokens[i + 1].start);
			power = atoi(strBuffer);
			if (power < 1) {
				err = JSON_POWER_FAIL;
				break;
			}
			i++;
		}
	}

	if (err == JSON_OK) {
		hystProfiles[profile-1].max_temp = maxTemp;
		hystProfiles[profile-1].min_temp = minTemp;
		hystProfiles[profile-1].power = power;
		hystProfiles[profile-1].changed= true;
		if (saveFlag == true) WriteValuesToEEprom();
	}

	sendDefaultResponse_Json(SET_HYST__JSON_TYPE_NAME, err);
}

void makeConfig_Json() {

	extern hysteresis hystProfiles[MAX_PROFILES];

	uint8_t count;
	char jsonBuffer[BUFFER_SIZE] = { 0 };
	char* dest = jsonBuffer;

	dest = json_objOpen(dest, NULL);
	dest = json_str(dest, TYPE__JSON_OBJECT_NAME, GET_CFG__JSON_TYPE_NAME);
	dest = json_int(dest, PROFILE__JSON_OBJECT_NAME, profileIndex + 1);
	dest = json_arrOpen(dest, PROFILES__JSON_OBJECT_NAME);
	for (count = 0; count < MAX_PROFILES; count ++) {
		dest = json_objOpen(dest, NULL);
		dest = json_int(dest, ID__JSON_OBJECT_NAME, count + 1);
		dest = json_int(dest, MAX_TEMP__JSON_OBJECT_NAME, hystProfiles[count].max_temp);
		dest = json_int(dest, MIN_TEMP__JSON_OBJECT_NAME, hystProfiles[count].min_temp);
		dest = json_int(dest, POWER__JSON_OBJECT_NAME, hystProfiles[count].power);
		dest = json_objClose(dest);
	}
	dest = json_arrClose(dest);
	dest = json_objClose(dest);

	json_end(dest);

	sendMessage(jsonBuffer);
}

void getTemp_Json() {

	extern 	Temperature temperature
	;
	char str[10] = { 0 };
	char jsonBuffer[BUFFER_SIZE] = { 0 };
	char* dest = jsonBuffer;

	snprintf(str,sizeof(str),"%u.%u", temperature.inCelsus, temperature.frac);

	dest = json_objOpen(dest, NULL);
	dest = json_str(dest, TYPE__JSON_OBJECT_NAME, TEMP_SENSOR__JSON_TYPE_NAME);
	dest = json_str(dest, TEMP__JSON_OBJECT_NAME, str);
	dest = json_objClose(dest);

	json_end(dest);

	sendMessage(jsonBuffer);
}

void sendDefaultResponse_Json(const char* messType, uint8_t err) {

	char strBuffer[256] = { 0 };

	sprintf(strBuffer, "{\"type\":\"%s\",\"status\":\"%s\"}", messType, JSON_ERRORS_STR[err]);
	sendMessage(strBuffer);
}

void sendMessage(char * message) {

	char strTemp[BUFFER_SIZE] = { 0 };

	sprintf(strTemp, "%s\n\r", message);

	CDC_Transmit_FS(strTemp, strlen(strTemp));
	HAL_UART_Transmit(&huart1,(uint8_t*)strTemp,strlen(strTemp), HAL_MAX_DELAY);
}

static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
	if (tok->type == JSMN_STRING && (int)strlen(s) == tok->end - tok->start &&
	    strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
		return 0;
	}
	return -1;
}
