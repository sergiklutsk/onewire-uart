/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "OneWire.h"
#include "string.h"
#include "stdbool.h"
#include "eeprom.h"
#include "protocol.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//#define STATIC_POWER
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint8_t devices;
uint32_t pDelay = 300;
uint8_t sensor;
uint8_t i;
OneWire ow;
char *crcOK;
uint8_t profileIndex = 0;
uint8_t common_array_number ;
float hyst1_min_temp = 25.5;
float hyst1_max_temp = 26.5;
bool flag_hyst1;
uint16_t timer2_counter;
bool flag_timer2_counter;
hysteresis hystProfiles[MAX_PROFILES];
uint8_t flag_usb;
Temperature temperature;

char line_buffer[80];
int line_len = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM2_Init(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void MX_USART2_UART_Init(uint32_t);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void ReadEEprom() {
	uint32_t data_;
	EE_Read(EEP_CHECK_ADDR,&data_);
	if (data_!= CHECK_DATA)	{
		EE_Format();
		WriteDefaultsConstantToEEP();
		EE_Write(EEP_CHECK_ADDR,CHECK_DATA);
	}

	ReadConstantsFromEEP();
}

void WriteDefaultsConstantToEEP() {
	for (uint8_t i=0;i<MAX_PROFILES;i++) {
		EE_Write(EEP_HYST_MIN + i * 5, 25); //
		EE_Write(EEP_HYST_MAX + i * 5, 30); //
		EE_Write(EEP_HYST_POWER + i * 5, 1); //
	}
	EE_Write(EEP_PROFILE, 0);
}

void WriteValuesToEEprom() {
	for (uint8_t i=0;i<MAX_PROFILES;i++) {
		EE_Write(EEP_HYST_MIN + i * 5, hystProfiles[i].min_temp);
		EE_Write(EEP_HYST_MAX + i * 5, hystProfiles[i].max_temp);
		EE_Write(EEP_HYST_POWER + i * 5, hystProfiles[i].power);
	}
	EE_Write(EEP_PROFILE, profileIndex);
}

void ReadConstantsFromEEP(void) {
	uint32_t _data;

	for (uint8_t i=0;i<MAX_PROFILES;i++) {
		EE_Read(EEP_HYST_MIN + i*5, &_data);hystProfiles[i].min_temp = _data;
		EE_Read(EEP_HYST_MAX + i*5, &_data);hystProfiles[i].max_temp = _data;
		EE_Read(EEP_HYST_POWER + i*5, &_data);hystProfiles[i].power = _data;
	}
	EE_Read(EEP_PROFILE, &_data);
	profileIndex = (uint8_t)_data;
	if (profileIndex > MAX_PROFILES - 1) profileIndex = 0;
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

	char str[100];

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_USB_DEVICE_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */

  for (uint8_t i=0;i<10;i++) {
	  HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin,SET);
	  HAL_Delay(DELAY1_MS);
	  HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin,RESET);
	  HAL_Delay(DELAY1_MS);
  }

  HAL_TIM_Base_Start(&htim2);
  HAL_TIM_Base_Start_IT(&htim2);

  snprintf(str,sizeof(str),"Kernel started...\n\rget_ROMid function executed...\n\r");
  HAL_UART_Transmit(&huart1,(uint8_t*)str,strlen(str), HAL_MAX_DELAY);

  get_ROMid();
  ReadEEprom();

  for (uint8_t i = 0; i < devices; i++) {
	  LED_ONOFF();
	  switch ((ow.ids[i]).family) {
      case DS18B20:
    	  temperature = readTemperature(&ow, &ow.ids[i], 1);
    	  snprintf(str,sizeof(str),"DS18B20 index:%2u , Temperature: %3u.%2uC\n\r",i,temperature.inCelsus, temperature.frac);
    	  HAL_UART_Transmit(&huart1,(uint8_t*)str,strlen(str), HAL_MAX_DELAY);
    	  break;
      case DS18S20:
    	  temperature = readTemperature(&ow, &ow.ids[i], 1);
    	  snprintf(str,sizeof(str),"DS18S20 index:%2u , Temperature: %3u.%2uC\n\r",i,temperature.inCelsus, temperature.frac);
    	  HAL_UART_Transmit(&huart1,(uint8_t*)str,strlen(str), HAL_MAX_DELAY);
    	  break;
      case 0x00:
    	  break;
      default:
    	  snprintf(str,sizeof(str),"UNKNOWN Family:%x (SN: %x%x%x%x%x%x\n\r)",
    			  (ow.ids[i]).family, (ow.ids[i]).code[0],(ow.ids[i]).code[1],(ow.ids[i]).code[2],(ow.ids[i]).code[3], (ow.ids[i]).code[4], (ow.ids[i]).code[5]);
    	  HAL_UART_Transmit(&huart1,(uint8_t*)str,strlen(str), HAL_MAX_DELAY);
    	  break;
	  }
  }

  snprintf(str,sizeof(str),"get_ROMid function complete.\n\r");
  HAL_UART_Transmit(&huart1,(uint8_t*)str,strlen(str), HAL_MAX_DELAY);
#ifdef STATIC_POWER
  SetPower(1);
#endif
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1) {
#ifndef STATIC_POWER
		  temperature = readTemperature(&ow, &ow.ids[0], 1);
		  CheckAndSetPower((float)temperature.inCelsus+temperature.frac/10);
		  HAL_Delay(500);
//	  }
#endif
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 64000;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 199;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, BUTTON_UP_Pin|BUTTON_ONOFF_Pin|BUTTON_DOWN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : LED_Pin */
  GPIO_InitStruct.Pin = LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : BUTTON_UP_Pin BUTTON_ONOFF_Pin BUTTON_DOWN_Pin */
  GPIO_InitStruct.Pin = BUTTON_UP_Pin|BUTTON_ONOFF_Pin|BUTTON_DOWN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void button_press(uint8_t button) {
	switch (button)	{
	case ONOFF_BUTTON:
		HAL_GPIO_WritePin(BUTTON_ONOFF_GPIO_Port,BUTTON_ONOFF_Pin,SET);
		HAL_Delay(DELAY1_MS);
		HAL_GPIO_WritePin(BUTTON_ONOFF_GPIO_Port,BUTTON_ONOFF_Pin,RESET);
		HAL_Delay(DELAY1_MS);
		break;
	case DOWN_BUTTON:
		HAL_GPIO_WritePin(BUTTON_DOWN_GPIO_Port,BUTTON_DOWN_Pin,SET);
		HAL_Delay(DELAY1_MS);
		HAL_GPIO_WritePin(BUTTON_DOWN_GPIO_Port,BUTTON_DOWN_Pin,RESET);
		HAL_Delay(DELAY1_MS);
		break;
	}
}

void SetPower(uint8_t power) {
	for (uint8_t i = 0; i< 6 - power;i++) {
		button_press(DOWN_BUTTON);
		HAL_Delay(DELAY1_MS);
	}
}

void get_ROMid (void) {
	char str[100];

	if (owResetCmd() != ONEWIRE_NOBODY)	{    // is anybody on the bus?
		devices = owSearchCmd(&ow);        // получить ROMid всех устройст на шине или вернуть код ошибки
		if (devices <= 0) {
			snprintf(str,sizeof(str),"Error has happened!");
			HAL_UART_Transmit(&huart1,(uint8_t*)str,strlen(str), HAL_MAX_DELAY);
			while (1) {
				pDelay = 1000000;
				for (i = 0; i < pDelay * 1; i++){}    /* Wait a bit. */
           // __asm__("nop");
			}
		}

		snprintf(str,sizeof(str),"found %d devices on 1-wire bus", devices);
		HAL_UART_Transmit(&huart1,(uint8_t*)str,strlen(str), HAL_MAX_DELAY);

		for (i = 0; i < devices; i++) {
			RomCode *r = &ow.ids[i];
			uint8_t crc = owCRC8(r);
			crcOK = (crc == r->crc)?"CRC OK":"CRC ERROR!";
			snprintf(str,sizeof(str),"\n\rdevice %d (SN: %02X/%02X%02X%02X%02X%02X%02X/%02X) ", i,
					r->family, r->code[5], r->code[4], r->code[3],r->code[2], r->code[1], r->code[0], r->crc);
			HAL_UART_Transmit(&huart1,(uint8_t*)str,strlen(str), HAL_MAX_DELAY);
			if (crc != r->crc) {
				snprintf(str,sizeof(str),"CRCfailedk\n\r");
				HAL_UART_Transmit(&huart1,(uint8_t*)str,strlen(str), HAL_MAX_DELAY);
			} else {
				snprintf(str,sizeof(str),"CRCok\n\r");
				HAL_UART_Transmit(&huart1,(uint8_t*)str,strlen(str), HAL_MAX_DELAY);
			}
		}
	}
}

void CheckAndSetPower(float celsium) {

	char str[512];

	if (celsium < hystProfiles[profileIndex].min_temp + 0.1) {
		if (!flag_hyst1) {
			snprintf(str,sizeof(str),"Set power to %d\n\r",hystProfiles[profileIndex].power);
			HAL_UART_Transmit(&huart1,(uint8_t*)str,strlen(str), HAL_MAX_DELAY);
			CDC_Transmit_FS(str, strlen(str));

			button_press(ONOFF_BUTTON);
			SetPower(hystProfiles[profileIndex].power);
			flag_hyst1 = true;
		}
	}

	if ((celsium > hystProfiles[profileIndex].max_temp - 0.1) & (flag_hyst1)) {
		snprintf(str,sizeof(str),"Power off.\n\r");
		HAL_UART_Transmit(&huart1,(uint8_t*)str,strlen(str), HAL_MAX_DELAY);
		CDC_Transmit_FS(str, strlen(str));

		button_press(ONOFF_BUTTON);
		flag_hyst1 = false;
	}
}

void MX_USART2_UART_Init(uint32_t baud) {

  huart2.Instance = USART2;
  huart2.Init.BaudRate = baud;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;

  if (HAL_HalfDuplex_Init(&huart2) != HAL_OK)
  {
	  Error_Handler();
  }

}

Temperature readTemp(uint8_t id) {
	return readTemperature(&ow, &ow.ids[id], 1);
}

uint8_t USB_ReceiveData_Callback (uint8_t* buf, uint32_t *len)
{
	char str_rx[1024] = {0};

	HAL_UART_Transmit(&huart1,(uint8_t*)buf,*len, HAL_MAX_DELAY);

    memcpy(line_buffer + line_len, buf, *len);
    line_len += *len;

    char* line_feed = memchr(buf, '\n', line_len);
    if (line_feed != NULL) {
        *line_feed = '\0';
    	strncpy(str_rx,(char*)line_buffer,line_len);
    	HAL_UART_Transmit(&huart1,(uint8_t*)str_rx,strlen(str_rx), HAL_MAX_DELAY);
    	if (jsonCommandsParser((char*)str_rx) == JSON_FAIL ) {

    	}
    	line_len = 0;
    }

    return USBD_OK;
}

void process_line(char* line) {
    // do something with the line; string functions are ok
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
