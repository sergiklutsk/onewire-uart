################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/OneWire.c \
../Src/eeprom.c \
../Src/json-maker.c \
../Src/main.c \
../Src/protocol.c \
../Src/stm32f1xx_hal_msp.c \
../Src/stm32f1xx_it.c \
../Src/syscalls.c \
../Src/system_stm32f1xx.c \
../Src/usb_device.c \
../Src/usbd_cdc_if.c \
../Src/usbd_conf.c \
../Src/usbd_desc.c 

OBJS += \
./Src/OneWire.o \
./Src/eeprom.o \
./Src/json-maker.o \
./Src/main.o \
./Src/protocol.o \
./Src/stm32f1xx_hal_msp.o \
./Src/stm32f1xx_it.o \
./Src/syscalls.o \
./Src/system_stm32f1xx.o \
./Src/usb_device.o \
./Src/usbd_cdc_if.o \
./Src/usbd_conf.o \
./Src/usbd_desc.o 

C_DEPS += \
./Src/OneWire.d \
./Src/eeprom.d \
./Src/json-maker.d \
./Src/main.d \
./Src/protocol.d \
./Src/stm32f1xx_hal_msp.d \
./Src/stm32f1xx_it.d \
./Src/syscalls.d \
./Src/system_stm32f1xx.d \
./Src/usb_device.d \
./Src/usbd_cdc_if.d \
./Src/usbd_conf.d \
./Src/usbd_desc.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c Src/subdir.mk
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32F103xB -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/ST/STM32_USB_Device_Library/Core/Inc -I../Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Src

clean-Src:
	-$(RM) ./Src/OneWire.d ./Src/OneWire.o ./Src/eeprom.d ./Src/eeprom.o ./Src/json-maker.d ./Src/json-maker.o ./Src/main.d ./Src/main.o ./Src/protocol.d ./Src/protocol.o ./Src/stm32f1xx_hal_msp.d ./Src/stm32f1xx_hal_msp.o ./Src/stm32f1xx_it.d ./Src/stm32f1xx_it.o ./Src/syscalls.d ./Src/syscalls.o ./Src/system_stm32f1xx.d ./Src/system_stm32f1xx.o ./Src/usb_device.d ./Src/usb_device.o ./Src/usbd_cdc_if.d ./Src/usbd_cdc_if.o ./Src/usbd_conf.d ./Src/usbd_conf.o ./Src/usbd_desc.d ./Src/usbd_desc.o

.PHONY: clean-Src

